class Queue:
    size=0
    q=list()

    def __init__(self,size):
        self.size=size

    def is_full(self):
        if len(self.q)==size:
            return True
        else:
            return False

    def is_empty(self):
        if len(self.q)==0:
            return True
        else:
            return False
        

    def enqueue(self,val):
        self.q.append(val)

    def dequeue(self):
        self.q.pop(0)
        
    def front(self):
        print("front element is : ",self.q[0])

    def rear(self):
        print("rear element is : ",self.q[-1])




size=int(input("please enter size of queue : \n"))
queue=Queue(size)

choice=1
while choice:
    print("\n\npress 1 for enqueue")
    print("press 2 for dequeue")
    print("press 3 for front element")
    print("press 4 for rear element")
    print("press 0 for exit")
    choice=int(input("please enter your choice : \n"))

    if choice==0:
        continue
    
    if choice==1:  #enqueue
        if not queue.is_full():
            val=input("please enter value you want to enter : \n")
            queue.enqueue(int(val))
        else:
            print("queue is full\n")

    elif choice==2: #dequeue
        if not queue.is_empty():
            queue.dequeue()
        else:
            print("queue is empty\n")

    elif choice==3: #front
        if not queue.is_empty():
            queue.front()
        else:
            print("queue is empty\n")

    elif choice == 4: #rear
        if not queue.is_empty():
            queue.rear()
        else:
            print("queue is empty\n")

    else:
        print("wrong choice\n")

