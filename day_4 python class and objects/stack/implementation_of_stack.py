class Stack:
    size=0
    s=list()
    
    def __init__(self,size):
        self.size=size

    def is_full(self):
        if len(self.s)<self.size:
            return False
        else:
            return True

    def is_empty(self):
        if len(self.s)==0:
            return True
        else:
            return False

    def push(self,n):
        self.s.append(n)
    
    def pop(self):
        self.s.pop()

    def peek(self):
        print("top of the stack is : ",self.s[-1],"\n")




    
size=int(input("please enter size of stack : "))
st=Stack(size)

choice=1
while choice:
    print("press 1 for push")
    print("press 2 for pop")
    print("press 3 for peek")
    print("press 0 for exit")
    choice=int(input("please enter your choice : \n"))

    if choice==0:
        continue
    
    if choice==1:  #push
        if not st.is_full():
            val=input("please enter value you want to enter : \n")
            st.push(int(val))
        else:
            print("stack is full\n")

    elif choice==2: #pop
        if not st.is_empty():
            st.pop()
        else:
            print("stack is empty\n")

    elif choice==3: #peek
        if not st.is_empty():
            st.peek()
        else:
            print("stack is empty\n")

    else:
        print("wrong choice\n")

