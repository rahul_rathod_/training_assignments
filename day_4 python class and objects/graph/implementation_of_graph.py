class Graph:
    grph=dict()
    visited=set()
    v=0
    e=0

    def __init__(self,vertex,edge):
        self.v=vertex
        self.e=edge

    
    def addedge(self,s,d):
        if self.grph.get(s)==None:
            self.grph[s]=[d]
        else:
            self.grph[s].append(d)


    def printgrph(self):
        for k in self.grph:
            print(k," is connected to : ",end="")
            for l in self.grph.get(k):
                print(l," ",end="")
            print("\n")

    def bfs(self,i):
        q=list()
        self.visited.add(i)
        q.append(i)

        while len(q)!=0:
            print(q[0]," ",end="")
            if self.grph.get(q[0])!=None:
                for i in self.grph.get(q[0]):
                    if i not in self.visited:
                        self.visited.add(i)
                        q.append(i)
            q.pop(0)

        self.visited.clear()



    def dfs(self,i):
        print(i," ",end=" ")

        self.visited.add(i)

        if self.grph.get(i)!=None:
            for element in self.grph.get(i):
                if element not in self.visited:
                    self.dfs(element)
        
        

g=Graph(15,14)

for i in range(g.e):
    val=input("enter edge (v1 and v2)")
    g.addedge(int(val.split()[0]),int(val.split()[1]))       

g.printgrph()
n=int(input("\non which element you want to apply BFS : \n"))
g.bfs(n)
n=int(input("\non which element you want to apply DFS : \n"))
g.dfs(n)
    
