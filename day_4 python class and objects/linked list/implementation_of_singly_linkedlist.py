class Node:
    data=0
    next=None

    def __init__(self,value):
        self.data=value
        self.next=None

class Linkedlist:

    head=None
    lptr=None

    def append(self):
        val=int(input("please enter element you want to enter : "))
        newnode=Node(val)

        if self.head==None:
            self.lptr=self.head=newnode
        else:
            self.lptr.next=newnode
            self.lptr=newnode

    
    def pop(self):
        if self.head==None:
            print("linked list is empty\n")

        elif self.head.next==None:
            del self.head
            self.head=None

        else:
            temp=self.head

            while temp.next != self.lptr:
                temp=temp.next
            
            temp.next=None
            del self.lptr
            self.lptr=temp




    def insert(self):
        pos=int(input("please enter position you want to enter node : "))
        
        if pos==1:
            if self.head==None:
                val=int(input("please enter element you want to enter : "))
                newnode=Node(val)
                self.head=newnode
                self.lptr=newnode
            else:
                val=int(input("please enter element you want to enter : "))
                newnode=Node(val)
                newnode.next=self.head
                self.head=newnode
                self.lptr=newnode
        else:
            temp=self.head
            while pos !=2:
                temp=temp.next
                pos=pos-1

            val=int(input("please enter element you want to enter : "))
            newnode=Node(val)
            newnode.next=temp.next
            temp.next=newnode

            if newnode.next==None:
                self.lptr=newnode


    def remove(self):
        
        if self.head==None:
            print("linked list is empty\n")
        else:
            pos=int(input("please enter position of node you want to remove : "))

            if pos==1:
                temp=self.head
                self.head=self.head.next
                del temp

            else:
                temp=self.head
                while pos!=2:
                    temp=temp.next
                    pos=pos-1

                if temp.next!=None:
                    temp.next=temp.next.next



    def printll(self):
        temp=self.head

        while temp!=None:
            print((temp.data)," ",end="")
            temp=temp.next
        
        print("\n")


ll=Linkedlist()

choice=1
while choice:
    print("\n\npress 1 for append node to linked list")
    print("press 2 for pop node from linked list")
    print("press 3 for insert node from specific position to linked list")
    print("press 4 for remove node from specific position to linked list")
    print("press 5 for print linked list")
    print("press 0 for exit")

    choice=int(input("please enter your choice\n"))


    if choice == 0 :
        continue

    elif choice == 1:
        ll.append()

    elif choice == 2:
        ll.pop()

    elif choice == 3:
        ll.insert()

    elif choice == 4:
        ll.remove()

    elif choice == 5:
        ll.printll()