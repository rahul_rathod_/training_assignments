

class Node:
    data=0
    left=None
    right=None

    def __init__(self):
        self.data=0
        self.left=None
        self.right=None

    def left(self):
        return self.left

    def right(self):
        return self.right



class Tree:

    head=None

    def build(self,temp,q):
        if temp==None:
            if len(q)==0:
                return None

            temp=Node()
            temp.data=q[0]
            temp.left=None
            temp.right=None
            q.pop(0)
            return temp


        temp.left=self.build(temp.left,q)
        temp.right=self.build(temp.right,q)
        return temp



    def insert(self,q):

        temp=self.head

        while len(q)!=0:
            temp=self.build(temp,q)

        return temp

    def inorder(self,temp):
        if temp==None:
            return
            
        self.inorder(temp.left)
        print(temp.data," ",end="")
        self.inorder(temp.right)




q=list()
n=int(input("how many element you want to enter in tree? : "))
print("please enter elements")
for i in range(n):
    val=int(input())
    q.append(val)

tree=Tree()
tree.head=tree.insert(q)
tree.inorder(tree.head)


