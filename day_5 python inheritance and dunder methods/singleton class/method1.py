class singleton:
    value="rahul"
    _instance=None
    
    def __init__(self):
        pass

    def __new__(cls,self):
        if not self._instance:
            return cls.__new__(cls)
        else:
            return cls._instance



o1=singleton()
print(o1.value)

o2=singleton()
print(o2.value)

o1.value="changed"

print(o1.value)
print(o2.value)