class base:

    def base(self):
        print("this is base class")

class a(base):
    
    def feature1(self):
        print("feature one is working")


class b(base):

    def feature2(self):
        print("feature two is working")

class c(a,b):

    def feature3(self):
        print("feature three is working")

ob=c()

ob.base()
ob.feature1()
ob.feature2()
ob.feature3()