class a:
    
    def feature1(self):
        print("feature one is working")


class b(a):

    def feature2(self):
        print("feature two is working")

class c(a):

    def feature3(self):
        print("feature three is working")

obc=c()

obc.feature1()
obc.feature3()

print("-"*10)

obb=b()
obb.feature1()
obb.feature2()
