var fn=document.getElementById("fn");
var ln=document.getElementById("ln");
var mail=document.getElementById("mail");
var phone=document.getElementById("ph");
var gn=document.form.gender;
var tnc=document.getElementById("chkbx");
var date=document.getElementById("date");
var month=document.getElementById("month");
var year=document.getElementById("year");
var gender;


function checkradio()
{
    var flag=false;
    for(var x=0;x<gn.length;x++)
    {
        if(gn[x].checked==true)
        {
            flag=true;
            gender=gn[x].value;
            break;
        }
    }
    return !flag;
}

function checkbirthdate()
{
    if(date.options[date.selectedIndex].value=="" || month.options[month.selectedIndex].value=="" || year.options[year.selectedIndex].value=="")
    {
        return true;
    }
    else
    {
        return false;
    }
}


function validate()
{
    if(fn.value=="")
    {
        fn.style.border="2px solid Red";
    }
    else
    {
        fn.style.border="none";
    }

    if(ln.value=="")
    {
        ln.style.border="2px solid Red";
    }
    else
    {
        ln.style.border="none";
    }

    if(mail.value=="")
    {
        mail.style.border="2px solid Red";
    }
    else
    {
        mail.style.border="none";
    }

    if(phone.value=="")
    {
        phone.style.border="2px solid Red";
    }
    else
    {
        phone.style.border="none";
    }

    if(checkradio())
    {
        document.getElementById("male").style.outline="2px solid red";
        document.getElementById("female").style.outline="2px solid red";
    }
    else
    {
        document.getElementById("male").style.outline="none";
        document.getElementById("female").style.outline="none";

    }

    if(tnc.checked==false)
    {
        tnc.style.outline="2px solid Red";
    }
    else
    {
        tnc.style.outline="none";
    }



    if(checkbirthdate())
    {
        document.getElementById("date").style.outline="2px solid red";
        document.getElementById("month").style.outline="2px solid red";
        document.getElementById("year").style.outline="2px solid red";
    }
    else
    {
        document.getElementById("date").style.outline="none";
        document.getElementById("month").style.outline="none";
        document.getElementById("year").style.outline="none";
    }

    if(fn.value!="" && ln.value!="" && mail.value!="" && phone.value!="" && checkradio()==false && tnc.checked==true && date.options[date.selectedIndex].value!="" && month.options[month.selectedIndex].value!="" && year.options[year.selectedIndex].value!="")
    {
        document.write("first name : "+fn.value+"<br>");
        document.write("last name : "+ln.value+"<br>");
        document.write("email id : "+mail.value+"<br>");
        document.write("phone number : "+phone.value+"<br>");
        document.write("gender : "+gender+"<br>");
        document.write("birthdate : "+date.options[date.selectedIndex].value+" "+month.options[month.selectedIndex].value+" "+year.options[year.selectedIndex].value)

        return true;
    }
    else
    {
        return false;
    }
    
}

