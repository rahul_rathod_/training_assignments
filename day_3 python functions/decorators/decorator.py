def f1(func):
    def wrapper(*args,**kargs):
        print("started")
        func(*args,**kargs)
        print("ended")

    return wrapper


@f1
def f(a,b=9):
    print(a,b)

f("hello")