d={1:"a",2:"b",3:"c"}

for key in d:
    print(key)


for val in d.values():
    print(val)

for key,value in d.items():
    print(key,value)

for key in d:
    print(key,d.get(key))


print(1 in d)

print(d.get(1))

